/* 
ToWorkflow - John Tseng

The MIT License (MIT)

Copyright (c) 2013 John Tseng

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


angular.module("ToWorkflow", ['firebase', 'JSONedit'])
.config(function($routeProvider) {
    $routeProvider
        .when('/workflows', {
            resolve:{name:function(){return 'WorkflowList'}},
            templateUrl: '/workflows-list.html', 
            controller: 'WorkflowsController'})
        .when('/tasks/:taskId', {
            resolve:{name:function(){return 'TaskDetail'}},
            templateUrl: '/task-detail.html',
            controller:"TaskDetailController"})
        .when('/tasks', {
            resolve:{name:function(){return 'TaskList'}},
            templateUrl: '/task-list.html',
            controller: "TaskListController"})
        .when('/data', {
            resolve:{name:function(){return 'AllData'}},
            templateUrl: '/data.html'})
        .when('/login', {
            resolve:{name:function(){return 'Login'}},
            templateUrl: '/login.html',
            controller: "LoginController"})
        .otherwise({redirectTo: '/tasks'});
})
.run(function(FireAuth, $location, $rootScope) {
    //ask for FireAuth so that it runs at least once.
})
.factory('FireAuth', function($rootScope, angularFire, $location, $route) {
    var detachCallbacks = function() {};
    return new FirebaseSimpleLogin(new Firebase("https://toworklist.firebaseio.com/"), function(error, user) {
        if (error) {
            console.log(error);
            toastr.error(error.message);
            $location.path('/login');
        } else if (user) {
            toastr.info('Successfully logged in as ' + user.email + '.');
            if ($route.current.locals.name === 'Login')
                $location.path('/');
            
            detachCallbacks();
            var rootRef = new Firebase("https://toworklist.firebaseio.com/" + user.id);
            rootRef.on('value', function(snapshot){
                if (snapshot.val() === null) {
                    rootRef.set({
                        workflows: {Simple: {steps: [{name: "New"}, {name:"Done"}]}},
                        tasks: {
                            "hidden":{hidden: true}, 
                            "first":{
                                name: "First Task",
                                workflow: "Simple",
                                step: "New",
                                description: "This is the first task."
                            }}
                    });
                }
            });
            detachCallbacks = function() {
                rootRef.off();
            };
            angularFire(rootRef, $rootScope, 'data');
            $rootScope.user = user;
        } else {
            detachCallbacks();
            if ($rootScope.user) toastr.info("Logged out. :'(");
            $location.path('/login');
            $rootScope.user = null;
        }
    });
})
.controller("LoginController", function($scope, FireAuth, $rootScope) {
    FireAuth.logout();
    console.log(FireAuth);
    $scope.Create = function() {
        FireAuth.createUser($scope.email, $scope.password, function(error, user) {
            console.log([error, user]);
            if (!error) {
                toastr.info(user.email + " was successfully registered!");
                $scope.Login();
            } else if (error.code === "EMAIL_TAKEN") {
                toastr.error($scope.email + " is already taken. Please try logging in.");
            } else {
                toastr.error('There was an error registering the user.');
            }
        });
    };
    $scope.Login = function() {
        FireAuth.login('password', {
            email: $scope.email,
            password: $scope.password, 
            rememberMe: $scope.rememberMe
        });
    };
})
.controller("TaskListController", function($scope, NextStep, $rootScope) {
    $scope.NextStep = function(task) {
        return NextStep(task.workflow, task.step);
    };
    $scope.Delete = function(id) {
        delete $rootScope.data.tasks[id];
    };
})
.controller("TaskDetailController", function($scope, $rootScope, $routeParams) {
    $scope.id = $routeParams.taskId;
    // $rootScope.$watch('data', function(newVal) {
    //     if (typeof(newVal) !== "undefined") {
    //         $scope.task = newVal.tasks[$routeParams.taskId];
    //         $scope.workflow = newVal.workflows[$scope.task.workflow];
    //     }
    // });
    $scope.Save = function() {
        toastr.info("Saved!");
    };
})
.controller('WorkflowsController', function($scope, $rootScope, guid, $location) {
    $rootScope.$watch('data', function(newVal) {
        if (typeof(newVal) !== "undefined") {
            $scope.workflows = newVal.workflows;
            $scope.tasks = newVal.tasks;
        }
    });
   
    $scope.AddWorkflow = function() {
        var name = $scope.newWorkflowName;
        $scope.workflows[name] = {
            steps: [{name:"New"}, {name:"Done"}]
        };
        $scope.newWorkflowName = "";
        console.log($rootScope);
    };
    
    $scope.AddTask = function(workflowName) {
        var newTaskName = prompt("Task name:", "New Task");
        var newTask = {
            name: newTaskName,
            workflow: workflowName,
            step: $scope.workflows[workflowName].steps[0].name,
            description: "This is a new task."
        };
        $scope.tasks[guid()] = newTask;
    //   $location.path("/tasks");
        toastr.info(newTaskName + " created!");
      
    };

    $scope.DeleteWorkflow = function(name) {
        delete $scope.workflows[name];
    };
})
.controller("navigationController", function($scope, $route){
    $scope.route = function(){
        if (typeof($route.current) === 'undefined') return '';
        if (typeof($route.current.locals.name) === 'undefined') return '';
        return $route.current.locals.name;
    };
    $scope.current = function(){return $route.current};
})
.factory('guid', function() {
    return function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {var r = Math.random()*16|0,v=c=='x'?r:r&0x3|0x8;return v.toString(16);});
    }
})
.factory('WorkflowStepIndex', function($rootScope, Workflow) {
    return function(workflowName, stepName) {
        var workflow = Workflow(workflowName);
        if (workflow === null) return null;
        
        var index = -1;
        angular.forEach(workflow.steps, function(step, i) {
            if (step.name === stepName)
                index = i;
        });
        return index;
    };
})
.factory('NextStep', function($rootScope, WorkflowStepIndex, Workflow) {
    return function(workflowName, stepName) {
        var workflow = Workflow(workflowName);
        if (workflow === null) return null;
        
        var nextStepIndex = WorkflowStepIndex(workflowName, stepName) + 1;
        
        if (nextStepIndex >= workflow.steps.length) return null;
        
        return workflow.steps[nextStepIndex].name;
    };
})
.factory('Workflow', function($rootScope) {
    return function(workflowName) {
        if (typeof($rootScope.data) === "undefined") return null;
        if (typeof($rootScope.data.workflows) === "undefined") return null;
        if (typeof($rootScope.data.workflows[workflowName]) === "undefined") return null;
        return $rootScope.data.workflows[workflowName];
    };
})
;